#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{

  if (argc != 3) {
    perror("[ERROR] : Usage %s <path_to_module> <nb_max>\n", argv[0]);
    return -1;
  }

  int file, new_value;

  // Opening file to write new value
  file = open(argv[1], O_WRONLY);
  if(file < 0){
    printf("Error opening file %s!\n",argv[1]);
    return -2;
  }
  
  new_value = atoi(argv[2]);
  if (!(0 < new_value && new_value < 256)) {
    perror("[ERROR] : second argument must be char in range [1..255]\n");
    return -3;
  } 

  int size = write(file, &new_value, sizeof(char));
  close(file);

  printf("[INFO] : New value is set to %d", new_value);
  return 0;
}
