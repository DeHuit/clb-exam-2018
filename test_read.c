#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
  if (argc != 2) {
    printf("[ERROR] : Usage %s <path_to_module>\n", argv[0]);
    return -1;
  }

  int file, new_value;

  // Opening file to read value
  file = open(argv[1], O_RDONLY);
  if(file < 0){
    printf("Error opening file %s!\n",argv[1]);
    return -2;
  }
  
  char a[5];
  int size = read(file, a, sizeof(char));
  close(file);

  printf("[INFO] : Button pushed\n");
  return 0;
}
